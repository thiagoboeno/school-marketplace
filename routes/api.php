<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/', function () {
//     return view('index');
// });

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout', 'UserController@logout');

    Route::prefix('products')->group(function () {
        Route::post('/', 'ProductController@store');
        Route::delete('/{id}', 'ProductController@destroy');
        Route::put('/{id}', 'ProductController@update');
    });

    Route::prefix('categories')->group(function () {
        Route::post('/', 'CategoryController@store');
        Route::delete('/{id}', 'CategoryController@destroy');
        Route::put('/{id}', 'CategoryController@update');
    });
});

Route::prefix('products')->group(function () {
    Route::get('/', 'ProductController@index');
    Route::get('/{id}', 'ProductController@show');
});

Route::prefix('categories')->group(function () {
    Route::get('/', 'CategoryController@index');
    Route::get('/{id}', 'CategoryController@show');
});
