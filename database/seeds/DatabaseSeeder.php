<?php

use App\User;
use App\Product;
use App\Category;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Mochilas','Estojos','Uniformes','Lápis','Canetas & Lapiseiras','Borrachas','Corretivos','Cola','Fichario','Marca Texto','Tessoura','Régra','Compasso & Esquadro','Cadernos','Livros'];

        foreach ($categories as $category) {
            factory(Category::class)->create(['name' => $category]);
        }

        factory(User::class)->create(['name' => 'Thiago Boeno', 'user_role' => 'A']);
        factory(User::class, 10)->create();

        $users = User::all();

        $users->each(function($u) {
            factory(Product::class, 5)->create(
                ['user_id' => $u->id]
            );
        });
    }
}
