<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct(Category $categoryModel) {
        $this->categoryModel = $categoryModel;
    }

    public function index() {
        $categories = $this->categoryModel->all();

        return response()->json([
            'success' => true,
            'data' => $categories
        ], 200);
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required|string',
            'parent_id' => 'nullable|integer'
        ]);

        $result = $this->categoryModel->create($request->all());

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Category registred!'
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Category can\'t be registred!'
            ], 400);
        }
    }

    public function show($id) {
        $category = $this->categoryModel->find($id);

        return response()->json([
            'success' => true,
            'data' => $category
        ], 200);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'name' => 'nullable|string',
            'parent_id' => 'nullable|integer'
        ]);

        $category = $this->categoryModel->find($id);

        $category->name = $request->name ? $request->name : $category->name;
        $category->parent_id = $request->parent_id ? $request->parent_id : $category->parent_id;

        $result = $category->save();

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Category updated!'
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Category can\'t be updated!'
            ], 400);
        }
    }

    public function destroy($id) {
        $category = $this->categoryModel->find($id);

        $result = $category->delete();

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Category deleted!'
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Category can\'t be deleted!'
            ], 400);
        }
    }
}
