<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct(Product $productModel) {
        $this->productModel = $productModel;
    }

    public function index() {
        $products = $this->productModel->all();

        return response()->json([
            'success' => true,
            'data' => $products
        ], 200);
    }

    public function store(Request $request) {
        $request->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'content' => 'required|string',
        ]);

        $request->slug = Str::slug($request->title);

        $result = $this->productModel->create($request->all());

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Product registred!'
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Product can\'t be registred!'
            ], 400);
        }
    }

    public function show($id) {
        $product = $this->productModel->find($id);

        $product->view_count = ++$product->view_count;
        $product->save();

        return response()->json([
            'success' => true,
            'data' => $product
        ], 200);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'title' => 'nullable|string',
            'description' => 'nullable|string',
            'content' => 'nullable|string',
        ]);

        $product = $this->productModel->find($id);

        $product->title = $request->title ? $request->title : $product->title;
        $product->slug = $request->title ? Str::slug($request->title) : $product->slug;
        $product->description = $request->description ? $request->description : $product->description;
        $product->content = $request->content ? $request->content : $product->content;

        $result = $product->save();

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Product updated!'
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Product can\'t be registred!'
            ], 400);
        }
    }

    public function destroy($id) {
        $product = $this->productModel->find($id);

        $result = $product->delete();

        if ($result) {
            return response()->json([
                'success' => true,
                'message' => 'Product deleted!'
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Product can\'t be deleted!'
            ], 400);
        }
    }
}
